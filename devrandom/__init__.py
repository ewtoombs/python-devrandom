import random

_DEBUG = False

class DevRandom(random.Random):
    def __init__(self, source_file = '/dev/random'):
        """Uses the random bits from source_file."""

        # Source must be private. I could have a method to change the source 
        # halfway through, but the leftover bits must be cleared.
        self._source = open(source_file, 'rb')
        # Number of leftover bits will always be under 8.
        self.leftover = 0
        self.nleftover = 0

    def getrandbits(self, k):
        """Gets bits from the source file."""

        # I deal in bytes first, then settle the remaining bits last. This 
        # way, sub-byte groups of bits are dealt with less and bytes are split 
        # less. The order is changed, but the bits are random and the order 
        # changing does not depend on the value of the bits, so everything 
        # remains perfectly random. For example, if k = 8*n, where n > 0, the 
        # leftover bits need never be accessed and no bytes need to be split.  
        # This would be impossible if nleftover were >0 and the bits were 
        # loaded strictly in order.
        (d, r) = divmod(k, 8)
        res = 0 # result
        for b in self._source.read(d):
            res <<= 8
            res |= b

        # Take care of the remainder, r.
        if r > self.nleftover:
            # One more read is needed.
            res <<= self.nleftover
            res |= self.leftover
            r -= self.nleftover

            self.leftover = self._source.read(1)[0]
            self.nleftover = 8
            # r > 0 right now, so even though nleftover is 8 right now, it 
            # won't be for very long. After the remaining r bit(s) are loaded 
            # from the replenished leftover bin, nleftover will be back under 
            # 8 again, as required.
        res <<= r
        # a bitmask. r 1s on the far right, with zeros everywhere else.
        bm = ((1 << r) - 1)
        res |= self.leftover & bm
        self.leftover >>= r
        self.nleftover -= r
        return res

    def getrandbit(self):
        """A specialisation of getrandbits() for getting just one bit."""
        if self.nleftover == 0:
            self.leftover = self._source.read(1)[0]
            self.nleftover = 8
            # Even though nleftover is 8 right now, it won't be for very long.  
            # After the bit is loaded from the replenished leftover bin, 
            # nleftover will be back under 8 again, as required.
        res = self.leftover & 1
        self.leftover >>= 1
        self.nleftover -= 1

        #print('leftover = %x; nleftover = %d' %
        #        (self.leftover, self.nleftover))
        return res

    def random(self):
        """Not implemented in this derived class.
           
        random() is implemented by  default with the Mersenne twister directly 
        instead of through getrandbits(), which is incredibly stupid. I don't 
        need it, so I disabled it to make sure none of the other methods like 
        choice(), randrange(), or randint() try to use the Mersenne twister 
        instead of bits from /dev/random or whatever the device is that is 
        specified.  I'll implement it myself with getrandbits() if I ever need 
        it.  If I do, the implementation should actually be in the base class.
           
        """
        raise NotImplementedError()

    # This version generates the number R in [0, N) effectively by retrieving 
    # an a-bit number, where N is in (2**(a-1), 2**a], and starting over if 
    # the resulting number is not in [0, N). This ensures a perfectly even 
    # distribution in [0, N).  It saves entropy by generating the number bit 
    # by bit with random bits retrieved from getrandbit() starting with the 
    # most significant bit.  If one of the bit addings guarantees R will be 
    # out of range, the algorithm is restarted before any more bits are 
    # generated.
    def _randbelow(self, N, countbits = False):
        """Returns (R, nbits), where nbits is the number of bits used to 
           generate the random number, when countbits == True."""
        if _DEBUG:
            print('My _randbelow was used. N = {}'.format(N))

        if countbits:
            if N == 1:
                return (0, 0)

            # Find e = 2**(a-1). Here, N > 1.
            e = 2
            while e < N:
                e <<= 1
            # Now, e >= N, so N is in (e/2, e] and e = 2**a.
            e >>= 1
            # Now, N is in (e, 2*e]. e = 2**(a-1).

            nbits = 0
            while True:
                outofrange = False
                f = e
                R = 0
                c = 0
                while f > 0:
                    R |= f*self.getrandbit()
                    c += 1
                    if R >= N:
                        outofrange = True
                        break
                    f >>= 1
                if outofrange:
                    nbits += c
                    continue
                break
            return (R, nbits)

        else:
            if N == 1:
                return 0

            # Find e = 2**(a-1). Here, N > 1.
            e = 2
            while e < N:
                e <<= 1
            # Now, e >= N, so N is in (e/2, e] and e = 2**a.
            e >>= 1
            # Now, N is in (e, 2*e]. e = 2**(a-1).

            while True:
                outofrange = False
                f = e
                R = 0
                while f > 0:
                    R |= f*self.getrandbit()
                    if R >= N:
                        outofrange = True
                        break
                    f >>= 1
                if outofrange:
                    continue
                break
            return R
