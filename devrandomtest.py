import devrandom
import itertools as i

import devrandom
r = devrandom.DevRandom('/dev/urandom')

h = list(i.repeat(0, 100))

T = 0
N = 1 << 20
for i in range(N):
    (R, nbits) = r._randbelow((1 << 32) + 1, True)
    T += nbits
print(T/N)
