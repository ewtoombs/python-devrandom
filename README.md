# python-devrandom

`/dev/random` support for python's `random` module. Configures the module to use
`/dev/random` as its entropy source, so that it generates truly random results,
instead of merely pseudorandom results. Cryptographically secure. Similar to
`random.SystemRandom`, only it uses `/dev/random` as a source instead of
`/dev/urandom`, and it can be configured to use any file as an entropy source
by changing the default `source_file` argument in the constructor.

## installing
Installing is pretty standard python fare. Combine `./setup.py install 
--root=$prefix` with the package manager of your choice. Some crazy person 
maintains an AUR package, so you Arch Linux users can do `yay -S
python-devrandom`.

## using
`python-devrandom` implements the `random.Random` abstract class as
`devrandom.DevRandom`. See python's documentation on this abstract class for
instructions on using it. Example:
```
import devrandom
r = devrandom.DevRandom()

print(r.choice(['spam', 'eggs', 'beans', 'sausages']))
```
